# Nativery SDK

### _**Enabler for Rich Media Ads**_

<hr>

### Come installare la libreria?
Per facilitare l'utilizzo della libreria gli esempi di seguito mostrano sia una versione con la forma di `Studio Enabler` che
con il solo `Nativery SDK`.
Lo script contente il Nativery SDK si trova a questo indirizzo url https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js 
e va importato nell'ad attraverso un tag `<script>` 

```js
<script src="https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js"></script>
```

o una chiamata `fetch`

```js
const response = await fetch(
    'https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js'
)
const script = await response.text()
eval(script)
```

### Come inizializzare l'Enabler?
A questo punto sarà possibile accedere nello script alla variabile `sdk` che conterrà sia la classe `Enabler` che 
gli `events`.
E' possibile passare al costruttore dell'`Enabler` un parametro `logger` di tipo boolean per mostrare in console alcune informazioni di debug.

```js
const Nativery = sdk.Nativery
Enabler = new Nativery.Enabler({
    logger: true
})
// questo esempio mantiene la struttura di Studio Enabler, altrimenti accedere direttamente a Nativery.events
studio = { events: { StudioEvent: Nativery.events } }
```

A questo punto basta aggiungere il `listener` di `INIT` che si occuperà di connettersi con il contesto in ascolto degli 
eventi dal di fuori dell'`iframe`. Non c'è bisogno di usare un `window.onload` poiché questo verrebbe ignorato 
da `Chrome` [[documentazione]](https://stackoverflow.com/questions/20572734/load-event-not-firing-when-iframe-is-loaded-in-chrome).
All'interno della libreria avviene già un controllo per determinare quando l'iframe è effettivamente disponibile e solo 
in quel caso viene eseguita la callback.

```js
Enabler.addEventListener(
    studio.events.StudioEvent.INIT, // Nativery.events.INIT
    enablerInitHandler // La funzione di callback da eseguire quando l'Enabler è inizializzato
)
```

### Cosa posso tracciare?
Sostanzialmente tre tipologie di eventi che vanno aggiunti sulla dashboard di Nativery all'interno dell'ad che si 
sta creando:
- **Exit**
  - _params_: `nameEvent`
  - _desc_: evento da lanciare al click sulla CTA
    - `Enabler.exit('Exit CTA')`
- **Counter**
  - _params_: `nameEvent` e `cumulative`
  - _desc_: contatore incrementale o meno sulla base del parametro `cumulative`
    - `Enabler.counter('Hotspot1Click', false)`
    - `Enabler.counter('Hotspot2Click', true)`
- **Timer**
  - _params_: `nameEvent`
  - _desc_: tracciamento delle tempistiche, da attivare e fermare al termine dell'attività da monitorare
    - `Enabler.startTimer('Timer1')`
    - `Enabler.stopTimer('Timer1')`
