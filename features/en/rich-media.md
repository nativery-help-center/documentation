# Nativery SDK

### _**Enabler for Rich Media Ads**_

<hr>

### How to install?
To facilitate the use of the library, the examples below show both a version with the `Studio Enabler` format and with only the `Nativery SDK`.  
The script containing the Nativery SDK can be found at this URL:  
https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js  
and should be imported into the ad using a `<script>` tag:

```js
<script src="https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js"></script>
```

or a `fetch` call:

```js
const response = await fetch(
    'https://cdn.nativery.com/widget/js/rm/nativery.sdk.external.js'
)
const script = await response.text()
eval(script)
```

### Initialize the Enabler
At this point, you can access the `sdk` variable in the script, which will contain both the `Enabler` class and the `events`.  
It is possible to pass a `logger` boolean parameter to the `Enabler` constructor to display some debug information in the console.

```js
const Nativery = sdk.Nativery
Enabler = new Nativery.Enabler({
    logger: true
})
// this example keeps the structure of Studio Enabler, otherwise access Nativery.events directly
studio = { events: { StudioEvent: Nativery.events } }
```

At this point, simply add the `INIT` `listener`, which will take care of connecting with the context listening for events from outside the `iframe`.  
There is no need to use a `window.onload` since it would be ignored by `Chrome`  
[[documentation]](https://stackoverflow.com/questions/20572734/load-event-not-firing-when-iframe-is-loaded-in-chrome).  
The library already includes a check to determine when the iframe is actually available, and only then is the callback executed.

```js
Enabler.addEventListener(
    studio.events.StudioEvent.INIT, // Nativery.events.INIT
    enablerInitHandler // The callback function to execute when the Enabler is initialized
)
```

### What can I track?

There are essentially three types of events that need to be added to the Nativery dashboard within the ad being created:
- **Exit**
    - _params_: `nameEvent`
    - _desc_: event triggered by clicking the CTA
        - `Enabler.exit('Exit CTA')`
- **Counter**
    - _params_: `nameEvent` and `cumulative`
    - _desc_: incremental or non-incremental counter based on the `cumulative` parameter
        - `Enabler.counter('Hotspot1Click', false)`
        - `Enabler.counter('Hotspot2Click', true)`
- **Timer**
    - _params_: `nameEvent`
    - _desc_: timing tracking, to be started and stopped when the activity to monitor ends
        - `Enabler.startTimer('Timer1')`
        - `Enabler.stopTimer('Timer1')`
