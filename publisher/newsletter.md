# Nativery Newsletter

Returns a list of JSON objects containing all the required information to compose a creativity.

### Endpoint

```bash
GET https://api2.nativery.com/dem/ads
```

### Parameters

- `widget`
    - **required** - The widget id. This field is needed to track users clicks.
- `limit`
    - **optional** - Can set a max number of creatives returned. Default is set to 5 and max to 30.
- `size`
    - **optional** - Can set the width size of the image returned (default is 350px). Size must be one of the following values: 200, 250, 300, 350, 400, 450, 500, 550, 600.

### Returns
An array of up to `limit` creatives having expiration date the farthest.
Each entry in the array is a separate creativity object. 
If no more creatives are available, the resulting array will be empty. 
This request should never return an error.

### Response

```bash
[
    {
        "title": "Creativity Title",
        "branding_text": "Branding Name",
        "image": "url-creativity-image",
        "url": "url-user-to-click"
    },
    {...},
    {...}
]
```