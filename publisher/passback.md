# Nativery Passback

Publishers can easily enable a **Passback** feature on their widgets allowing different strategies when **Nativery** isn't 
able to serve any creatives.
In order to activate the **Passback** is necessary to correctly set the corresponding dashboard's option in the widget panel.

### Privacy

Depending on the privacy contest, **Nativery** can support both _consent_ and _no-consent_ scenery.
In edit mode, switch on the "_Active Passback_" button under the "_Passback Options_" section and select the appropriate 
choice based on the privacy policy context where the widget will appear.

### No-Consent context
When the widget is on page where user does not agree with the cookies' policy, **Nativery** can emit an event to notify the 
publisher when a creatives blank fill occurs.
In this case choose the "_Custom Event_" option from the select type, and it'll be shown a tooltip on how to implement this
on your website. 

The event emitted has this structure:

```node
new CustomEvent('nativeryHasNoAds', { detail: { widgetId: "The widget id" } });
```

And can be intercepted with an event listener with this structure:

```node
window.addEventListener('nativeryHasNoAds', (e) => {
  console.log(`The widget id is: ${e.detail.widgetId}`);
}, false);
```

### Consent context
The only consent **Passback** that currently **Nativery** supports is the **Google Publisher Tag (GPT)**.
Choose the "_Google Publisher Tag_" option from the select type to active it and provide the requested information.
The required fields are "_Ad Unit Path_", "_Size_" and "_Div ID_". These fields information are provided from your Ad Server, 
check [this](https://developers.google.com/publisher-tag/guides/get-started) guide if you need help.
In order to properly works the Google macro "**PATTERN:url**" (further information [here](https://support.google.com/admanager/answer/2376981?hl=en#zippy=%2Cpattern-match)) have to be added in the Nativery's script inserted directly 
in the ad unit, as shown below:

```node
_nat.push(['id','WIDGET_ID',{'GAM_REFERRER': '%%PATTERN:url%%'}]);
```

When triggered all Nativery's code will be removed and replaced with functionalities as shown below:

```html
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<div id="Div ID">
<script>
  window.googletag = window.googletag || {cmd: []};
  googletag.cmd.push(function() {
    googletag.defineSlot("Ad Unit Path", "Size", "Div ID")
      .addService(googletag.pubads());
    googletag.enableServices();
    googletag.pubads().setCentering(true);
    googletag.pubads().set("page_url", "Domain retrieved from macro");
    googletag.display("Div ID");
  });
  </script>
</div>
```